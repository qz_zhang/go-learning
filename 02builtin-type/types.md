## Go数组
go的数组和C++中的类似，一旦声明长度固定。
```go
// delcalration
var scores [10]int
//
socres := [10]int{1,3,}

// 迭代遍历
for idx, val := range scores {

}
```

## Go切片（Dynamic Array）
```go
// delcaration
sc := []int{1,2}
// cap is 10, len is 0
sc := make([]int, 10)
// cap is 10, len is 4
sc := make([]int, 4, 10) 
```

## Go映射（哈希表）
```go
// map[key_type]value_type
m := map[string]int
```

# Go的struct
## 声明及赋值
值得一说，Go中没有类以及继承的概念。
```go
// declaration
type Person struct {
    Name string
    Age int
}

// 赋值
zhangsan := Person {
    Name: "zs",
    Age: 22, // note this
}

lisi := Person{}
lisi.Name = "ls"
lisi.Age = 14

wangwu := Person{"ww", 26}
```
## 将结构体与函数关联
```go
type Person struct {
    Name string
    Age int
}
// 该方法的调用者为Person类型的变量
func (p *Person) PrintPerson() {
    // print relevant information of person p
}
```

## new函数
```go
p1 := new(Person)
// 与下面等价
p1 := &Person{}   
```

## 套娃（组合）
```go
type Person struct {
    Name string
}

func (p *Person)Introduce() {
    println("I am: " + p.Name)
}

type Worker struct {
    *Person
    JobType string
}

w := &Worker {
    Person: &Person{"zhangsan"},
    JobType: "programmer",
} 
```