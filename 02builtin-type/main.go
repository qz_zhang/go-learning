package main

import "fmt"

type Person struct {
	Name string
	Age  int
}

// person printer
func (p *Person) PrintPerson() {
	println("Name: " + p.Name)
	fmt.Printf("Age: %d\n", p.Age)
}

type Worker struct {
	*Person
	JobType string
}

func main() {
	p := Person{
		"zhangsan",
		20,
	}
	p.PrintPerson()
	w := &Worker{
		Person:  &Person{"lisi", 0},
		JobType: "programmer",
	}
	println(w.JobType)
	w.Person.PrintPerson()
}
