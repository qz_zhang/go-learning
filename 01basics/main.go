package main

import "fmt"

func add(a, b, c int) int {
	return a + b + c
}

func main() {
	fmt.Println(add(1, 3, 2))
}
