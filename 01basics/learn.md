# Go基础

## 声明
```go
// 声明一个变量
var a int // (var name type)
// declare a int variable
var a int = 9
// 声明并赋值
a := 9
// 多变量赋值
name, age := "zhangsan", 22
```


## 运行
首先Go的程序入口必须是main包内的main函数，例如以下的代码：
```go
package main

import fmt

func main() {
    fmt.Println("hello Go")
}
```
执行该程序可以使用命令`go run main.go`或者先进行编译`go build main.go`然后执行代码。

## 函数声明
```go
// no return value
func log(msg string) {

}
// one return value
func add(a int, b int) int {

}
// two return values
func test(a string) (int, bool) {

}
// three params's type is same
func add1(a,b,c int) {
    
} 
```